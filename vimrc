set nocompatible
filetype off
set t_Co=256
" set rtp+=~/.vim/bundle/Vundle.vim/
" call vundle#begin()
"
call plug#begin('~/.vim/bundle')

 " Bundles
" Plug 'gmarik/Vundle.vim'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-vinegar'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeFind' }
Plug 'scrooloose/syntastic'
Plug 'NLKNguyen/papercolor-theme'
" Plug 'jistr/vim-nerdtree-tabs.git'
Plug 'Valloric/YouCompleteMe'
" Plug 'gmarik/github-search.vim'
Plug 'gmarik/sudo-gui.vim'
Plug 'tpope/vim-fugitive'
" Plug 'Lokaltog/vim-easymotion'
Plug 'mhinz/vim-signify'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-eunuch'
Plug 'justinmk/vim-sneak'
Plug 'mxw/vim-jsx', { 'for': 'jsx' }
" Plug 'Chiel92/vim-autoformat'
Plug 'dyng/ctrlsf.vim', { 'on': 'CtrlSF' }
" Plug 'EasyGrep'
Plug 'terryma/vim-multiple-cursors'
" Plug 'DBGPavim'
Plug 'kien/ctrlp.vim'
Plug 'bling/vim-airline'
Plug 'altercation/vim-colors-solarized'
" Plug 'othree/javascript-libraries-syntax.vim'
" Plug 'jelera/vim-javascript-syntax'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
" Plug 'koron/nyancat-vim'
Plug 'Raimondi/delimitMate'
Plug 'docunext/closetag.vim', { 'for': 'html'}
" Plug 'stephenmckinney/vim-solarized-powerline'
Plug 'gregsexton/MatchTag', { 'for': 'html'}
Plug 'klen/python-mode', { 'for': 'py' }
Plug 'szw/vim-dict', { 'on': 'Dict'}
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-repeat'
" Plug 'tpope/vim-sleuth'
" Plug 'LargeFile'
" Plug 'TwitVim'
Plug 'mattn/emmet-vim', { 'for': ['html', 'htmldjango'] }
Plug 'tpope/vim-surround'
" Plug 'YankRing.vim'
" Plug 'teramako/jscomplete-vim'
Plug 'godlygeek/tabular', { 'on':  'Tabularize' }
Plug 'tpope/vim-markdown'
Plug 'marijnh/tern_for_vim', { 'for': 'javascript'}
" Plug 'Keithbsmiley/investigate.vim'
Plug 'zhaocai/GoldenView.Vim'
Plug 'mtth/scratch.vim'
" Plug 'davidhalter/jedi-vim'

call plug#end()
filetype plugin indent on

" command to run at startup
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

set nocp
set nowrap                       " dont wrap lines

"vertical/horizontal scroll off settings
set scrolloff=5
set sidescrolloff=7
set sidescroll=1

" set confirm                      " get a dialog when :q, :w, or :wq fails
set copyindent

" Display unprintable chars. Override vim-sensible defaults
set list
set listchars=tab:▸\ ,extends:❯,precedes:❮,nbsp:␣
set showbreak=↪

" Case insensitive search
set ignorecase
set smartcase
set tabstop=4
set shiftwidth=4
set expandtab
set nu
set title
set hlsearch
" Make regex a little easier to type
set magic
" set mouse=n               " use mouse in visual,insert and normal mode
set viminfo='10,\"100,:20,%,n~/.viminfo
set wildignore+=*.o,*.obj,*.class,.git,node_modules,*.png,*.jpg,*.gif,*.jpeg,*.log,logs,npm-debug.log

"undo files
set undodir=~/.vim/undodir
set undofile
set undolevels=1000 
set undoreload=10000
set cursorline

" fold conf
set foldmethod=indent
set foldlevelstart=1
" set nofoldenable

" Writes to the unnamed register also writes to the * and + registers. This
" makes it easy to interact with the system clipboard
if has ('unnamedplus')
  set clipboard=unnamedplus
else
  set clipboard=unnamed
endif
set pastetoggle=<F3>
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif
if ( has( "gui_running" ) )
    set guioptions-=m
    set guioptions-=r
    set guioptions-=L
    set guioptions-=T
    set mouse=n
    set guifont=Fira\ Mono\ Regular\ 11
endif

" for things that are particular to this user/computer,
" you can add commands to a .local_vim file in your home dir
" and uncommenting the following
" source .local_vim

" set vb
" let g:solarized_contrast="high"
" let g:solarized_termcolors=256
" let g:seoul256_background = 246
" let g:solarized_termtrans=1
syntax enable
" set background=light
" colorscheme solarized
colorscheme PaperColor

let g:airline_theme='PaperColor'
let g:goldenview__enable_default_mapping = 0
let g:netrw_liststyle=3

" let g:dbgPavimPort = 9081
let g:JSLintHighlightErrorLine=0
let g:html_indent_inctags="html,body,head,tbody"

let g:UltiSnipsUsePythonVersion = 2
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsEditSplit="vertical"
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_add_preview_to_completeopt = 1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_min_num_of_chars_for_completion = 0

" vim dict
let g:dict_hosts = [ ["dict.org", ["all"]] ]
" zen coding
let g:user_emmet_mode='i'
let g:use_emmet_complete_tag = 1
let g:user_emmet_expandabbr_key='<C-e>'
" airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#whitespace#enabled = 0
" let g:airline_powerline_fonts = 1

" Py-Mode configs
let g:pymode_rope = 1
let g:pymode_rope_complete_on_dot = 0

" Documentation
let g:pymode_doc = 1
let g:pymode_doc_key = 'K'

"Linting
let g:pymode_lint = 1
let g:pymode_lint_checker = "pyflakes,pep8"
" Auto check on save
let g:pymode_lint_write = 1

" Support virtualenv
let g:pymode_virtualenv = 1

" syntax highlighting
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all

" Git commits.
autocmd FileType gitcommit setlocal spell

map <C-b> :NERDTreeToggle<CR>
" window resize commands
" map - <C-W><
map + <C-W>>
" Move tab bindings
nmap <silent> <C-h> :wincmd h<CR>
nmap <silent> <C-j> :wincmd j<CR>
nmap <silent> <C-k> :wincmd k<CR>
nmap <silent> <C-l> :wincmd l<CR>

" Hightlight mappings
" Use <Space> to clear the highlighting of :set hlsearch.
if maparg('<Space>', 'n') ==# ''
  nnoremap <silent> <Space> :nohlsearch<CR><Space>
endif
" Quickly select text you just pasted
noremap gV `[v`]
nnoremap <Leader>t :tabnew<CR>
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
" make Y consistent with C and D
nnoremap Y y$
" <Leader>f: Open Quickfix
nnoremap <silent> <Leader>f :botright copen<CR>
" <Leader>n: NERDTreeFind
nnoremap <silent> <Leader>n :NERDTreeFind<cr>
" U: Redos since 'u' undos
nnoremap U <c-r>
" _ : Quick horizontal splits
nnoremap _ :sp<cr>
" | : Quick vertical splits
nmap <silent> <bar> <Plug>GoldenViewSplit
" nnoremap <bar> :vsp<cr>
" move line down
nnoremap <silent> <C-Down> :.m+<CR>
" move line up
nnoremap <silent> <C-Up> :-m.<CR>k
" Dont move cursor to enxt search
nnoremap * *<c-o>

" automatically open and close the popup menu / preview window
" au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
" set completeopt=menuone,menu,longest,preview
